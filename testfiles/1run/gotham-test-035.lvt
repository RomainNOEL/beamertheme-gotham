\input regression-test.tex\relax

%\documentclass{minimal}
\documentclass[aspectratio=169]{beamer}
\usefonttheme[shape title=smallcaps]{gotham}

   \title[shorttitle]{Title long}
	\subtitle[shortsubtitle]{Subtile long}
	\date[shortdate]{\today}
	\author[john.doe@mail.com]{John DOE\inst{1}}
	\institute[shortinstitute]{\inst{1} An Awesome Company}
	\titlegraphic{Logo-title}
	%\logo{LOGO}

\begin{document}
%\ExplSyntaxOn
\START
\showoutput


\TEST{test font features - 169 frame transparent}{
  %\gothamset{
  %   titleformat title=allcaps
  %}

  \begin{frame}[plain,noframenumbering]{test fonts}
		\begin{itemize}
			\item Regular
			\item \textit{Italic}
			\item \textsc{Small Caps}
			\item \textbf{Bold}
			\item \textbf{\textit{Bold Italic}}
			\item \textbf{\textsc{Bold Small Caps}}
			\item \texttt{Monospace}
			\item \texttt{\textit{Monospace Italic}}
			\item \texttt{\textbf{Monospace Bold}}
			\item \texttt{\textbf{\textit{Monospace Bold Italic}}}
		\end{itemize}
	\end{frame}
}


\TEST{option format title regular - shape title smallcaps 169 frame transparent}{
  %\gothamset{
  %   titleformat title=allcaps
  %}

  \begin{frame}[plain,noframenumbering]{title of a frame}
		\titlepage
	\end{frame}
}

\TEST{option format title upper - shape title smallcaps 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format title=upper%
   }
   
   \begin{frame}[plain,noframenumbering]{title of a frame}
		\titlepage
	\end{frame}
}

\TEST{option format title titlecase - shape title italic 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format title=titlecase, shape title=italic
   }

   \begin{frame}[plain,noframenumbering]{title of a frame}
		\titlepage
	\end{frame}
}

\TEST{option format title lower - shape title regular 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format title=lower, shape title=regular
   }

   \begin{frame}[plain,noframenumbering]{title of a frame}
		\titlepage
	\end{frame}
}

%\gothamset{
\SetKeys[gotham/font]{
   format title=regular, shape title=regular
}

\TEST{option format subtitle regular - shape subtitle regular 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format subtitle=regular, shape subtitle=regular
   }

   \begin{frame}[plain,noframenumbering]{SUBtitle of a frame}
		\titlepage
	\end{frame}
}

\TEST{option format subtitle upper - shape subtitle regular 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format subtitle=upper, shape subtitle=regular
   }

   \begin{frame}[plain,noframenumbering]{SUBtitle of a frame}
		\titlepage
	\end{frame}
}

\TEST{option format subtitle lower - shape subtitle regular 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format subtitle=lower, shape subtitle=italic
   }

   \begin{frame}[plain,noframenumbering]{SUBtitle of a frame}
		\titlepage
	\end{frame}
}

\TEST{option format subtitle titlecase - shape subtitle smallcaps 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format subtitle=titlecase, shape subtitle=smallcaps
   }

   \begin{frame}[plain,noframenumbering]{SUBtitle of a frame}
		\titlepage
	\end{frame}
}


\TEST{option format frametitle regular - shape frametitle regular 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format frametitle=regular, shape frametitle=regular,
      format framesubtitle= upper, shape framesubtitle=italic,
   }

   \begin{frame}{Frametitle of a frame}
   \framesubtitle{Subtitle frame}
		dummy frame with subtitle
	\end{frame}
}

\TEST{option format frametitle upper - shape frametitle regular 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format frametitle=upper, shape frametitle=regular,
      format framesubtitle= titlecase, shape framesubtitle=smallcaps,
   }

   \begin{frame}{Frametitle of a frame}
   \framesubtitle{Subtitle frame}
		dummy frame with subtitle
	\end{frame}
}

\TEST{option format frametitle lower - shape frametitle regular 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format frametitle=lower, shape frametitle=italic,
      format framesubtitle= regular, shape framesubtitle=italic,
   }

   \begin{frame}{Frametitle of a frame}
   \framesubtitle{Subtitle frame}
		dummy frame with subtitle
	\end{frame}
}

\TEST{option format frametitle titlecase - shape frametitle smallcaps 169 frame transparent}{
   %\gothamset{
   \SetKeys[gotham/font]{
      format frametitle=titlecase, shape frametitle=smallcaps,
      format framesubtitle= lower, shape framesubtitle=regular,
   }

   \begin{frame}{Frametitle of a frame}
   \framesubtitle{Subtitle frame}
		dummy frame with subtitle
	\end{frame}
}

% \TEST{option frametitle smallcaps 169 frame transparent}{
%    %\gothamset{
%    %   titleformat frame=allcaps
%    %}

%    \begin{frame}{Title}
%       \framesubtitle{Subtitle}
%       toto
%    \end{frame}
% }


%\END % comment this line to produce pdf and check visually the result
%\ExplSyntaxOff
\vfil\break
\end{document}
