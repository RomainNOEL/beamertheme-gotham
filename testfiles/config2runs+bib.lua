-- This is build-moreruns.lua
checksearch = true
checkruns = 2
testfiledir  = "testfiles/2runs+bib"

-- Additional test tasks
function runtest_tasks ( name , run )
   if run == 1 then
      return "biber --onlylog " .. name
   else
      return "echo "..0
   end
end
