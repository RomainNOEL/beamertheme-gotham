#!/bin/bash
echo -e "\n run_l3build: BEGIN" 

## Cleaning temporary files.
echo -e "\n l3build clean: start" 
if l3build clean ; then
   echo -e "\n l3build clean: done \n" 
else
   c=$?
   echo -e "\n l3build clean: FAILED \n exit $c"
   exit $c 
fi

## Unpacking source files.
echo -e "\n l3build unpack: start \n" 
if l3build unpack ; then
   echo -e "\n l3build unpack: done \n" 
else
   c=$?
   echo -e "\n l3build unpack: FAILED \n exit $c" 
   exit $c
fi

## Verify that the version tag is increasing.
echo -e "\n Checking Tag increase: start \n"
git fetch --tags
# Get the latest tag from Git
LATEST_TAG=$(git describe --tags $(git rev-list --tags --max-count=1) | sed 's/^v//g' || echo "0.0.0.0")
echo -e "> Latest git tag (version) found= $LATEST_TAG"
TAG=$(l3build pkgversion | grep "Version:" | sed 's/.*Version: \([0-9.]*[a-z]*\).*/\1/')
if [ -z "$TAG" ]; then 
   echo "Error: TAG from 'l3build pkgversion' is empty \n"; 
   exit 1; 
fi
echo -e "> l3build pkgversion (tag)= $TAG \n"
# Check if the new tag is greater than the latest tag
if [ "$TAG" = "$LATEST_TAG" ]; then 
   echo "Error: New tag and latest tag are identical"; 
   exit 1; 
fi
if [ $(echo -e "$LATEST_TAG\n$TAG" | sort -r -V | head -n1) != "$TAG" ]; then 
   echo "Error: New tag ($TAG) is not greater than the latest tag ($LATEST_TAG)"; 
   exit 1; 
else 
   echo -e " Checking Tag increase ($TAG): done \n"; 
fi

## Verify that files are correctly stamp with tag version.
echo -e "\n l3build checktag: start \n" 
if l3build checktag ; then
   echo -e "\n l3build checktag: done \n" 
else
   c=$?
   echo -e "\n l3build checktag: FAILED \n Please consider running 'l3build updatetag'\n exit $c" 
   exit $c
fi

# ## Verify that ChangeLog has new entries.
# echo -e "\n Check ChangeLog: TODO \n" 
# CI_API_V4_URL="https://gitlab.com/api/v4"
# CI_PROJECT_ID=52144864
# # LATEST_TAG=$(git describe --tags $(git rev-list --tags --max-count=1) | sed 's/^v//g' || echo "0.0.0.0")
# LATEST_TAG="0.1.0"
# CI_COMMIT_REF_NAME="main" #${LATEST_TAG}
# TAG_DATE="2024-09-27T23:27:47+02:00" #$(git show -s --format=%cI 0.1.0.a)
# url_var="$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/changelog?version=v1.2.0&from=init"
# # ?version=v1.2.0&date=${TAG_DATE}
# echo "url= $url_var"
# curl \
#    --url "$url_var" # | jq -r .notes >> CHANGELOG.md

## Generate documentation files.
echo -e "\n l3build doc: start \n" 
if l3build doc ; then
   echo -e "\n l3build doc: done \n"
else
   c=$?
   echo -e "\n l3build doc: FAILED \n exit $c"
   exit $c
fi

# ## Perform unit tests.
# echo -e "\n l3build check: start \n" 
# if l3build check -S ; then
#    echo -e "\n l3build check: done \n" 
# else
#    c=$?
#    echo -e "\n l3build check: FAILED \n exit $c" 
#    exit $c
# fi

## Generate a CTAN archive without an updated manifest.
echo -e "\n l3build ctan without manisfest: start \n"
if l3build ctan ; then
   echo -e "\n l3build ctan without manisfest: done \n" 
else
   c=$?
   echo -e "\n l3build ctan: FAILED \n exit $c" 
   exit $c
fi

## Update the manifest file.
echo -e "\n l3build manifest: start \n"
if l3build manifest ; then
   echo -e "\n l3build manifest: done \n" 
else
   c=$?
   echo -e "\n l3build manifest: FAILED \n exit $c" 
   exit $c
fi

## Generate a CTAN archive WITH an updated manifest.
echo -e "\n l3build ctan with manisfest included: start \n"
if l3build ctan ; then
   echo -e "\n l3build ctan with manisfest included: done \n \n Everything seems ready !\n You can publish on ctan your new version with the command:\n   l3build upload" 
else
   c=$?
   echo -e "\n l3build ctan with manisfest included: FAILED \n exit $c" 
   exit $c
fi

## Update the CTAN repository
# echo -e "\n l3build upload: start \n"
# if l3build upload -F ./ctan/ctan.ann --dry-run ; then
#    echo -e "\n l3build upload: DONE \n \n" 
# else
#    c=$?
#    echo -e "\n l3build upload: FAILED \n exit $c" 
#    exit $c
# fi

echo -e "\n run_l3build: END \n"
exit 0;
# EoF