#!/usr/bin/env texlua

--[[
  ** Build config for gotham using l3build **
--]]

-- Identification
module     = "gotham"
pkgversion = "1.2.1.c" -- Major, Minor, Patch, Tweak
pkgdate    = "2025-02-11"

-- 
-- CONFIGURATION of files for build and installation
-- ------------------------
maindir       = "." -- Top level directory for the module/bundle
sourcefiledir = "./src" -- Directory containing source files
sourcefiles   = {"*.dtx", "*.ins", "gotham-logo.pdf", "gotham-layout.pdf", "gotham-blueprint.pdf", "gotham-safetybox.pdf"} -- Files to copy for unpacking
installfiles  = {"*.sty", "*.cls", "*.pdf",} -- Files to install to the tex area of the texmf tree
supportdir = "./support" -- general support files
-- local fake_tds = "build/distrib" -- Define the fake TDS directory


-- 
-- UNPACKING
-- ------------------------

-- Unpacking files from .dtx file
unpackfiles = {""..module..".ins"} -- Files to run to perform unpacking
unpackopts  = "--interaction=batchmode" -- Options passed to engine when unpacking
-- unpackexe   = "luatex" -- Executable for running unpack
-- unpacksuppfiles= {""..module.."-logo.pdf", } -- Files needed to support unpacking when “sandboxed”


-- 
-- DOCUMENTATION + EXAMPLES
-- ------------------------

-- Generating documentation
-- docfiledir = "./doc" -- Directory containing documentation files
docfiles = {"examples/*.tex", "doc/"..module.."-doc.tex"} --Files which are part of the documentation and should be copy into doc.
demofiles = {"examples/*.tex"} -- Files which show how to use a module
-- typesetdemofiles = {"examples/"..module.."-example.tex"} -- Files to typeset before the documentation for inclusion in main documentation files

typesetfiles  = { ""..module.."-exampleSimple.tex", ""..module.."-example43dark.tex", ""..module.."-example43light.tex", ""..module.."-example169dark.tex", ""..module.."-example169transp.tex", ""..module..".dtx", ""..module.."-user-cmds.tex", ""..module.."-dev-impl.tex", ""..module.."-doc.tex", } --""..module..".ins", -- Files to typeset for documentation
typesetopts   = "-interaction=batchmode --shell-escape" -- Options passed to engine when typesetting
typesetsuppfiles = {""..module.."-logo.pdf", "*.png"} -- Files that match typesetsuppfiles in the support directory (supportdir) are copied into the build/doc directory


-- 
-- UNIT TESTS
-- ------------------------

testfiledir = "./testfiles/1run" -- Directory containing test files
-- testsuppdir = "./testfiles/support" -- Directory containing test-specific support files
checksearch = true -- Enable command printing during check phase
checkruns=1
checkconfigs = {"build", "./testfiles/config2runs", "./testfiles/config2runs+bib"}


-- 
-- PACKAGING FOR CTAN
-- ------------------------

-- textfiledir = maindir -- Directory containing plain text files
ctanInputFolder = maindir .."/ctan" -- Custom folder with files for CTAN 
textfiles = { "MANIFEST.md", "README.md", ctanInputFolder.."/*.*"} -- Plain text files to send to CTAN as-is

-- Update package date and version
tagfiles = {sourcefiledir.."/"..module..".dtx", "README.md", "doc/"..module.."-doc.tex", ctanInputFolder.."/ctan.ann"}

-- Configuration for ctan
-- ctanreadme = ctanInputFolder .."/CTANREADME.md" --
-- manifestfile = "MANIFEST.md" -- 
ctanpkg    = "beamertheme-"..module
-- ctandir = distribdir .. "/ctan"  -- Directory for organising the OUTPUT files for CTAN
ctanzip    = ctanpkg.."-"..pkgversion.."-ctan"
-- ctanfiles = {}
-- 
uploadconfig = {
  author      = "Romain NOEL",
  uploader    = "Romain NOEL",
  email       = "romainoel@free.fr",
  pkg         = ctanpkg,
  version     = pkgversion,
  license     = "lppl1.3c",
  summary     = "A modern, minimal-ish, versatile and extendable yet robust theme for Beamer",
  description = [[This package provides a modern, minimal-ish, versatile and extendable yet robust Beamer theme using LaTeX3 with some gathered or borrowed lines of code. 
  It uses l3build system to both build and verify (Test-Driven Development) the delivered code.
  `Gotham` tries to bring higher flexibility thanks to `LaTeX3` implementation on top of the good-looking `Metropolis` theme.]],
  topic       = { "presentation", "latex3", "expl3" }, -- "beamer" is not a topic
  ctanPath    = "/macros/latex/contrib/beamer-contrib/themes" .. ctanpkg,
  home        = "https://gitlab.com/RomainNOEL/beamertheme-gotham/",
  repository  = "https://gitlab.com/RomainNOEL/beamertheme-gotham/",
  development = "https://gitlab.com/RomainNOEL/beamertheme-gotham/",
  bugtracker  = "https://gitlab.com/RomainNOEL/beamertheme-gotham".."/issues",
  support     = "https://gitlab.com/RomainNOEL/beamertheme-gotham".."/issues",
  announcement_file = ctanInputFolder .."/ctan.ann",
  note_file   = ctanInputFolder .."/ctan.note",
  update      = true, -- does the pkg exists on CTAN
}


-- 
-- EXTRAS FUNCTIONS like TAGGING
-- -----------------------------

-- Clean files
cleanfiles = {
  ctanzip..".curlopt",
  ctanzip..".zip",
  "*.log",
  ""..module.."-example*.pdf",
  ""..module..".pdf",
  ""..module.."-*.pdf",
}

-- Define the files and their specific patterns
files2tag = {
  {
    name = "README.md",
    version_pattern = "Latest_Release%-v%d+%.%d+%.%d+",
    date_pattern = "Date: %d%d%d%d%-%d%d%-%d%d",
    version_output = "Latest_Release-v".. string.match(pkgversion, "(%d+%.%d+%.%d+)"),
    date_output = "Date: " .. pkgdate
  },
  {
    name = "src/gotham.dtx",
    version_pattern = "ProvidesExplPackage{beamer?%a+theme"..module.."}{%d%d%d%d%-%d%d%-%d%d}{%d+%.%d+%.%d+%.%w+}",
    version_subpattern = "}{%d+%.%d+%.%d+%.%w+}",
    date_pattern = "ProvidesExplPackage{beamer?%a+theme"..module.."}{%d%d%d%d%-%d%d%-%d%d}{%d+%.%d+%.%d+%.%w+}",
    date_subpattern = module.."}{%d%d%d%d%-%d%d%-%d%d}{",
    version_output =  "}{".. pkgversion .."}",
    date_output = module.."}{"..pkgdate.."}{"
  },
  {
    name = "src/gotham.dtx",
    version_pattern = "GothamVersion{v%d+%.%d+%.%d+%.%w+}",
    date_pattern = "GothamDate{%d%d%d%d%-%d%d%-%d%d}",
    version_output =  "GothamVersion{v".. pkgversion .."}",
    date_output = "GothamDate{"..pkgdate.."}"
  },
  {
    name = "doc/gotham-doc.tex",
    version_pattern = "GothamVersion{v%d+%.%d+%.%d+%.%w+}",
    date_pattern = "GothamDate{%d%d%d%d%-%d%d%-%d%d}",
    version_output =  "GothamVersion{v".. pkgversion .."}",
    date_output = "GothamDate{"..pkgdate.."}"
  },
  {
    name = "ctan/ctan.ann",
    version_pattern = "Version: %d+%.%d+%.%d+%.%w+",
    date_pattern = "Date: %d%d%d%d%-%d%d%-%d%d",
    version_output = "Version: "..pkgversion,
    date_output = "Date: "..pkgdate,
  },
  {
    name = "CHANGELOG.md",
    version_pattern = "Version: %d+%.%d+%.%d+%.%w+",
    date_pattern = "Date: %d%d%d%d%-%d%d%-%d%d",
    version_output = "Version: "..pkgversion,
    date_output = "Date: "..pkgdate,
    onlyone_check = true,
  }
}


-- Function to replace version and date in the target files
local function update_version_date(writeFilesBool)
  -- Assign default values if arguments are not provided (i.e., nil)
  writeFilesBool = writeFilesBool or true
  
  -- Iterate over each file and apply specific patterns
  for _, file in ipairs(files2tag) do
    -- Read the file content
    local f = io.open(file.name, "r")
    if not f then
      print("Error: Could not open file: " .. file.name)
      goto continue -- Skip to the next file
    end
    local content = f:read("*all")
    f:close()

    -- Track if changes were made
    local changes_made = false

    -- Verify and replace version
    if file.version_pattern then
      if string.match(content, file.version_pattern) then
        if file.version_subpattern then
          -- Replace only the version part
          content = string.gsub(content, file.version_pattern, 
            function(line)
              return string.gsub(line, file.version_subpattern, file.version_output)
            end
          )
        else  
          content = string.gsub(content, file.version_pattern, file.version_output)
        end
        changes_made = true
      else
        print("Error: Version pattern not recognized in file: " .. file.name)
      end
    end

    -- Verify and replace date
    if file.date_pattern then
      if string.match(content, file.date_pattern) then
        if file.date_subpattern then
          -- Replace only the date part, after version is already replaced
          content = string.gsub(content, file.date_pattern, 
            function(line)
              return string.gsub(line, file.date_subpattern, file.date_output)
            end
          )
        else
          content = string.gsub(content, file.date_pattern, file.date_output)
        end
        changes_made = true
      else
        print("Error: Date pattern not recognized in file: " .. file.name)
      end
    end

    -- If changes were made, write back to the file
    if changes_made then
      if file.onlyone_check then
        print("Warn: The version and date in: "..file.name.." is 'onlyone_check', ie. has to be updated MANUALLY !!" )
        -- os.exit(2)
      else
        f = io.open(file.name, "w")
        f:write(content)
        f:close()
        print("Updated version and date in: " .. file.name)
      end
    else
      print("No changes made to: " .. file.name)
    end

    ::continue::
  end
end

-- Create check_marked_tags() function
local function check_marked_tags()
  retCode=0

  -- Iterate over each file and apply specific patterns
  for _, file in ipairs(files2tag) do
    -- Read the file content
    local f = io.open(file.name, "r")
    if not f then
      print("Error: Could not open file: " .. file.name)
      goto continue -- Skip to the next file
    end
    local content = f:read("*all")
    f:close()

    -- Verify and replace version
    if file.version_pattern then
      retCodeTemp=0
      if string.match(content, file.version_pattern) then
        for line in string.gmatch(content, file.version_pattern) do
          if file.version_subpattern then
              lineOut = string.gsub(line, file.version_subpattern, file.version_output)
          else
            lineOut = string.gsub(line, file.version_pattern, file.version_output)
          end
          if line==lineOut then
            print("[OK] Version tag in file: " .. file.name.." are matching.")
            -- retCodeTemp=0
          else
            print("Warning: Unmatching version tag in file: " .. file.name .." at line: "..line.."\n")
            retCodeTemp=2
          end
          if file.onlyone_check then break end
        end
      else
        print("Warning: Version pattern not recognized in file: " .. file.name)
        retCodeTemp=2
      end
      retCode=math.max(retCode,retCodeTemp)
    end

    -- Verify and replace date
    if file.date_pattern then
      retCodeTemp=0
      if string.match(content, file.date_pattern) then
        for line in string.gmatch(content, file.date_pattern) do
          if file.date_subpattern then
              lineOut = string.gsub(line, file.date_subpattern, file.date_output)
          else
            lineOut = string.gsub(line, file.date_pattern, file.date_output)
          end
          if line==lineOut then
            print("[OK] Date tag in file: " .. file.name.." are matching.")
            -- retCodeTemp=0
          else
            print("Warning: Unmatching date tag in file: " .. file.name .." at line: "..line.."\n")
            retCodeTemp=2
          end
          if file.onlyone_check then break end
        end
      else
        print("Warning: Date pattern not recognized in file: " .. file.name)
        retCodeTemp=2
      end
      retCode=math.max(retCode,retCodeTemp)
    end

    ::continue::
  end

  os.exit(retCode)
end

-- Add the verification after the tag.
function tag_hook(tagname)
  print("INFO: previous tagging can be dry, so let's check again!")
  check_marked_tags()
end

-- Add "checktag" target to l3build CLI to verfy if tags are the same.
if options["target"] == "checktag" then
  retCode= check_marked_tags()
  os.exit(retCode)
end

-- Hook the function to a custom target
if options["target"] == "updatetag" then
  update_version_date()
  os.exit(0)
end

-- Add target to print the version of the package
if options["target"] == "pkgversion" then
  print("Package: "..module)
  print("Version: "..pkgversion)
  print("Date: "..pkgdate)
  os.exit(0)
end


-- EoF