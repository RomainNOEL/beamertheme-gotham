# Gotham Change Log


## Version: 1.2.1.c  Date: 2025-02-11

### Fixes
- spacing vertical and horizontal between subsections toc.
- spacing vertical for sections with `gotham bullet` toc leading to an error with `minted` package ([https://gitlab.com/RomainNOEL/beamertheme-gotham/-/issues/4]).
- use of `minted` instead of `listings` for code highlighting in examples.

### News
- add `\partpageOptions`, `\sectionpageOptions`, `\subsectionpageOptions`, `\subsubsectionpageOptions` controlling the frame options of parts and sections pages...
- add `\partpageTocOptions`, `\sectionpageTocOptions`, `\subsectionpageTocOptions`, `\subsubsectionpageTocOptions` controlling the frame options of the table of contents for parts and sections...
- add `\partTocOptions`, `\sectionTocOptions`, `\subsectionTocOptions`, `\subsubsectionTocOptions` controlling the options of table of contents for parts and sections...


## Version: 1.2.0.a  Date: 2024-11-11

### Breaking changes
- `standout template` option renamed to `standout BG template`.
- `\gothamFootlineOffset`,`\gotham@progressonsectionpage@linewidth`, `\gotham@frametitle@toppadding`, `\gotham@frametitle@bottompadding`, `\gotham@frametitle@leftpadding`, `\gotham@frametitle@rightpadding`  lengths renamed `\gothamFootlineVOffset`, `\gothamProgressSectionHeight`, `\gothamFrametitleToppading`, `\gothamFrametitleBottompading`, `\gothamFrametitleLeftpading`, `\gothamFrametitleRightpading`.

### News
- test with theorem from `thmtools`.
- add hook after colorset `\gothamHookPostColorBGSet`.
- add hook in footer `\gothamHookFooter` and add `\gothamFootlineHRightOffset` length.
- `standout template` option to change the whole page.

### Fixes
- length of subtitle in title page of gotham-normal and gotham-splitvert
- primary palette typo
- add vertically centered to `\maketitle`.


## Version: 1.1.0.e  Date: 2024-09-30

### News
- add mini-frames options
- improve progressbar manipulation

### Fixes
- boxes from metropolis
- combination of environments

### Breaking changes
- renamed options, especially 'numbering'


## Version: 1.0.0.a  Date: 2023-05-18

### News
- reimplementation using mainly LaTeX3
- new mechanism for changing templates mid-presentation (or when context is changing) 


## Version: 0.1.0.a  Date: 2023-12-20

### News
- first draft of the implementation using only LaTeX2e