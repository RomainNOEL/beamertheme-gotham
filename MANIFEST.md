# Manifest for gotham

This file is a listing of all files considered to be part of this package.
It is automatically generated with `l3build manifest`.


## Repository manifest

The following groups list the files included in the development repository of the package.
Files listed with a ‘†’ marker are included in the TDS but not CTAN files, and files listed
with ‘‡’ are included in both.

### Source files

These are source files for a number of purposes, including the `unpack` process which
generates the installation files of the package. Additional files included here will also
be installed for processing such as testing.

* gotham.dtx ‡
* gotham.ins ‡
* gotham-logo.pdf ‡
* gotham-layout.pdf ‡
* gotham-blueprint.pdf ‡
* gotham-safetybox.pdf ‡

### Text files

Plain text files included as documentation or metadata.

* MANIFEST.md ‡
* README.md ‡

### Derived files

The files created by ‘unpacking’ the package sources. This typically includes
`.sty` and `.cls` files created from DocStrip `.dtx` files.

* beamercolorthemegotham.sty †
* beamerfontthemegotham.sty †
* beamerinnerthemegotham.sty †
* beamerouterthemegotham.sty †
* beamerthemegotham.sty †
* gotham-blueprint.pdf ‡
* gotham-layout.pdf ‡
* gotham-logo.pdf ‡
* gotham-safetybox.pdf ‡

### Support files

These files are used for unpacking, typesetting, or checking purposes.

* gotham-example169transp-1.png 
* gotham-example43dark-1.png 
* gotham-exampleSimple-1.png 
* gotham-logo.png 
* gotham-test-035-a.png 
* gotham-test-035-b.png 

### Test files

These files form the test suite for the package. `.lvt` or `.lte` files are the individual
unit tests, and `.tlg` are the stored output for ensuring changes to the package produce
the same output. These output files are sometimes shared and sometime specific for
different engines (pdfTeX, XeTeX, LuaTeX, etc.).

* gotham-test-001.lvt 
* gotham-test-035.lvt 
* gotham-test-037.lvt 
* gotham-test-040.lvt 
* gotham-test-043.lvt 
* gotham-test-044.lvt 
* gotham-test-045.lvt 
* gotham-test-1002.lvt 
* gotham-test-1002b.lvt 
* gotham-test-1003.lvt 
* gotham-test-1003b.lvt 
* gotham-test-1004.lvt 
* gotham-test-1005.lvt 
* gotham-test-1006.lvt 
* gotham-test-1007.lvt 
* gotham-test-1007b.lvt 
* gotham-test-1008.lvt 
* gotham-test-1009.lvt 
* gotham-test-1010.lvt 
* gotham-test-1011.lvt 
* gotham-test-1012.lvt 
* gotham-test-1013.lvt 
* gotham-test-1014.lvt 
* gotham-test-1015.lvt 
* gotham-test-1016.lvt 
* gotham-test-1017.lvt 
* gotham-test-1018.lvt 
* gotham-test-1019.lvt 
* gotham-test-1020.lvt 
* gotham-test-1021.lvt 
* gotham-test-1022.lvt 
* gotham-test-1024.lvt 
* gotham-test-1025.lvt 
* gotham-test-1026.lvt 
* gotham-test-1027.lvt 
* gotham-test-1028.lvt 
* gotham-test-1031.lvt 
* gotham-test-1032.lvt 
* gotham-test-1037.lvt 
* gotham-test-1038.lvt 
* gotham-test-001.lve 
* gotham-test-035.luatex.tlg 
* gotham-test-035.tlg 
* gotham-test-035.xetex.tlg 
* gotham-test-037.luatex.tlg 
* gotham-test-037.tlg 
* gotham-test-037.xetex.tlg 
* gotham-test-040.luatex.tlg 
* gotham-test-040.tlg 
* gotham-test-040.xetex.tlg 
* gotham-test-043.luatex.tlg 
* gotham-test-043.tlg 
* gotham-test-043.xetex.tlg 
* gotham-test-044.luatex.tlg 
* gotham-test-044.tlg 
* gotham-test-044.xetex.tlg 
* gotham-test-045.luatex.tlg 
* gotham-test-045.tlg 
* gotham-test-045.xetex.tlg 
* gotham-test-1002.luatex.tlg 
* gotham-test-1002.tlg 
* gotham-test-1002.xetex.tlg 
* gotham-test-1002b.luatex.tlg 
* gotham-test-1002b.tlg 
* gotham-test-1002b.xetex.tlg 
* gotham-test-1003.luatex.tlg 
* gotham-test-1003.tlg 
* gotham-test-1003.xetex.tlg 
* gotham-test-1003b.luatex.tlg 
* gotham-test-1003b.tlg 
* gotham-test-1003b.xetex.tlg 
* gotham-test-1004.luatex.tlg 
* gotham-test-1004.tlg 
* gotham-test-1004.xetex.tlg 
* gotham-test-1005.luatex.tlg 
* gotham-test-1005.tlg 
* gotham-test-1005.xetex.tlg 
* gotham-test-1006.luatex.tlg 
* gotham-test-1006.tlg 
* gotham-test-1006.xetex.tlg 
* gotham-test-1007.luatex.tlg 
* gotham-test-1007.tlg 
* gotham-test-1007.xetex.tlg 
* gotham-test-1007b.luatex.tlg 
* gotham-test-1007b.tlg 
* gotham-test-1007b.xetex.tlg 
* gotham-test-1008.luatex.tlg 
* gotham-test-1008.tlg 
* gotham-test-1008.xetex.tlg 
* gotham-test-1009.luatex.tlg 
* gotham-test-1009.tlg 
* gotham-test-1009.xetex.tlg 
* gotham-test-1010.luatex.tlg 
* gotham-test-1010.tlg 
* gotham-test-1010.xetex.tlg 
* gotham-test-1011.luatex.tlg 
* gotham-test-1011.tlg 
* gotham-test-1011.xetex.tlg 
* gotham-test-1012.luatex.tlg 
* gotham-test-1012.tlg 
* gotham-test-1012.xetex.tlg 
* gotham-test-1013.luatex.tlg 
* gotham-test-1013.tlg 
* gotham-test-1013.xetex.tlg 
* gotham-test-1014.luatex.tlg 
* gotham-test-1014.tlg 
* gotham-test-1014.xetex.tlg 
* gotham-test-1015.luatex.tlg 
* gotham-test-1015.tlg 
* gotham-test-1015.xetex.tlg 
* gotham-test-1016.luatex.tlg 
* gotham-test-1016.tlg 
* gotham-test-1016.xetex.tlg 
* gotham-test-1017.luatex.tlg 
* gotham-test-1017.tlg 
* gotham-test-1017.xetex.tlg 
* gotham-test-1018.luatex.tlg 
* gotham-test-1018.tlg 
* gotham-test-1018.xetex.tlg 
* gotham-test-1019.luatex.tlg 
* gotham-test-1019.tlg 
* gotham-test-1019.xetex.tlg 
* gotham-test-1020.luatex.tlg 
* gotham-test-1020.tlg 
* gotham-test-1020.xetex.tlg 
* gotham-test-1021.luatex.tlg 
* gotham-test-1021.tlg 
* gotham-test-1021.xetex.tlg 
* gotham-test-1022.luatex.tlg 
* gotham-test-1022.tlg 
* gotham-test-1022.xetex.tlg 
* gotham-test-1024.luatex.tlg 
* gotham-test-1024.tlg 
* gotham-test-1024.xetex.tlg 
* gotham-test-1025.luatex.tlg 
* gotham-test-1025.tlg 
* gotham-test-1025.xetex.tlg 
* gotham-test-1026.luatex.tlg 
* gotham-test-1026.tlg 
* gotham-test-1026.xetex.tlg 
* gotham-test-1027.luatex.tlg 
* gotham-test-1027.tlg 
* gotham-test-1027.xetex.tlg 
* gotham-test-1028.luatex.tlg 
* gotham-test-1028.tlg 
* gotham-test-1028.xetex.tlg 
* gotham-test-1031.luatex.tlg 
* gotham-test-1031.tlg 
* gotham-test-1031.xetex.tlg 
* gotham-test-1032.luatex.tlg 
* gotham-test-1032.tlg 
* gotham-test-1032.xetex.tlg 
* gotham-test-1037.luatex.tlg 
* gotham-test-1037.tlg 
* gotham-test-1037.xetex.tlg 
* gotham-test-1038.luatex.tlg 
* gotham-test-1038.tlg 
* gotham-test-1038.xetex.tlg 


## TDS manifest

The following groups list the files included in the TeX Directory Structure used to install
the package into a TeX distribution.

### Source files (TDS)

All files included in the `gotham/source` directory.

* gotham.dtx 
* gotham.ins 

### TeX files (TDS)

All files included in the `gotham/tex` directory.

* beamercolorthemegotham.sty 
* beamerfontthemegotham.sty 
* beamerinnerthemegotham.sty 
* beamerouterthemegotham.sty 
* beamerthemegotham.sty 
* gotham-blueprint.pdf 
* gotham-layout.pdf 
* gotham-logo.pdf 
* gotham-safetybox.pdf 

### Doc files (TDS)

All files included in the `gotham/doc` directory.

* MANIFEST.md 
* README.md 
* ctan.ann 
* ctan.note 
* gotham-dev-impl.pdf 
* gotham-doc.pdf 
* gotham-doc.tex 
* gotham-example169transp.pdf 
* gotham-example169transp.tex 
* gotham-example43dark.pdf 
* gotham-example43dark.tex 
* gotham-exampleSimple.pdf 
* gotham-exampleSimple.tex 
* gotham-user-cmds.pdf 
* gotham.pdf 
* section-Beamer.tex 
* section-Conclusion.tex 
* section-Gotham.tex 


## CTAN manifest

The following group lists the files included in the CTAN package.
